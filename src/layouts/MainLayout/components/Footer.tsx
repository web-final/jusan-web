import styled from "styled-components";

export const Footer = () => {
    return (
        <Wrapper>
            <Container className="container">
                <DownloadBanner>
                    <QrCode>
                        <img src="https://jusan.kz/file-server/filename?dir=additional&filename=qr.webp" alt=""/>
                    </QrCode>
                    <Offer>
                        <h3>Приложение Jusan</h3>
                        <p>Одно приложение – много возможностей!</p>
                        <p>В приложении Jusan вы найдете все, что вам нужно</p>
                        <Apps>
                            <DownloadButton>Скачать</DownloadButton>
                        </Apps>
                    </Offer>
                </DownloadBanner>
                <BottomNav>
                    <BtmNavItem>О банке</BtmNavItem>
                    <BtmNavItem>Новости</BtmNavItem>
                    <BtmNavItem>Jusan analytics</BtmNavItem>
                    <BtmNavItem>Документы</BtmNavItem>
                    <BtmNavItem>Курсы валют</BtmNavItem>
                    <BtmNavItem>Отделения</BtmNavItem>
                    <BtmNavItem>Банки</BtmNavItem>
                </BottomNav>
            </Container>
        </Wrapper>
    )
}

const Wrapper = styled.div`
    background: white;
`

const Container = styled.div``

const DownloadBanner = styled.div`
    align-items: center;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin: auto;
    max-width: 1024px;
    padding-top: 20px;
`

const QrCode = styled.div`
  width: 130px;
  & img {
      border: 8px solid #f6f6f5;
      border-radius: 12px;
      width: 100%;
  }
`
const Offer = styled.div`
    background-image: url("https://jusan.kz/file-server/filename?dir=additional&filename=mobile.png");
    background-position-x: right;
    background-position-y: bottom;
    background-repeat: no-repeat;
    background-size: 220px;
    padding: 30px;
    position: relative;
    width: 100%;
    & h3 {
        font-size: 28px;
        font-weight: 700;
    }
    & p {
        font-size: 17px;
        margin: 0;
    }
`

const Apps = styled.div`
    margin-top: 20px;
`

const DownloadButton = styled.button`
    background: #1d1d1b;
    border: 0;
    border-radius: 18px;
    color: #fff;
    font-size: 17px;
    font-weight: 700;
    height: 50px;
    transition: box-shadow .3s ease-in-out,color .3s ease-in-out;
    width: 270px;
`;

const BottomNav = styled.nav`
    background: #f6f6f5;
    align-items: center;
    border-radius: 18px;
    color: black;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin: 0;
    padding: 16px 24px;
    margin-bottom: 24px;
`

const BtmNavItem = styled.a`
    display: flex;
    text-align: center;
`;
