import styled from "styled-components";
import {NavLink} from "react-router-dom";

export const Header = () => {
    const isAuth = false;

    return (
        <Wrapper>
            <Container className="container">
                <Top>
                    <LeftContainer>
                        <Logo src="https://jusan.kz/file-server/filename?dir=logo&filename=logo-desktop.png" />
                        <UpperNavigation>
                            <UpperNavItem to="/my-bank">Банк</UpperNavItem>
                            <UpperNavItem to="">Бизнес</UpperNavItem>
                            <UpperNavItem to="">Private</UpperNavItem>
                        </UpperNavigation>
                        {isAuth ? <Username /> : <LoginButton to="/login">Войти</LoginButton>}
                    </LeftContainer>
                </Top>
                <Bottom>
                    <BottomNavItem to="/shop">
                        <NavIcon src="https://jusan.kz/file-server/filename?dir=icons/header-tiny&filename=shop-tiny.webp" />
                        <span>Магазин</span>
                    </BottomNavItem>
                    <BottomNavItem to="/my-bank">
                        <NavIcon src="https://jusan.kz/file-server/filename?dir=icons/header-tiny&filename=invest-tiny.webp" />
                        <span>Мой банк</span>
                    </BottomNavItem>
                    <BottomNavItem to="/my-payments">
                        <NavIcon src="https://jusan.kz/file-server/filename?dir=icons/header-tiny&filename=inshurance-tiny.webp" />
                        <span>Платежи</span>
                    </BottomNavItem>
                    <BottomNavItem to="/">
                        <NavIcon src="https://jusan.kz/file-server/filename?dir=icons/header-tiny&filename=travel-tiny.webp" />
                        <span>Страхование</span>
                    </BottomNavItem>
                </Bottom>
            </Container>
        </Wrapper>
    )
}

const Wrapper = styled('header')`
    background: white;
    padding: 20px 0;
  color: #898d94;
  box-shadow: 0 0 10px #e1e1e1;

`;

const Container = styled('div')`
  
`
const Top = styled('div')`
    margin-bottom: 20px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const LeftContainer = styled('div')`
    display: flex;
  align-items: center;
  flex: 1;
`;

const RightContainer = styled('div')`
    display: flex;
  align-items: center;
`;

const Logo = styled('img')`
    margin-right: 30px;
  width: 160px;
`;

const UpperNavigation = styled('nav')`
    display: flex;
  align-items: center;
  column-gap: 16px;
  flex: 1;
`;

const UpperNavItem = styled(NavLink)`
`

const Bottom = styled('div')`
    display: flex;
  column-gap: 16px;
  align-items: center;
`;

const BottomNavItem = styled(NavLink)`
    display: flex;
    align-items: center;
  column-gap: 8px;
  font-size: 15px;
`;

const NavIcon = styled('img')`
    width: 34px;
  height: 34px;
`;

const LoginButton = styled(NavLink)`
    padding: 8px 10px;
  border-radius: 12px;
    background: #fd7e14;
    color: white;
    font-weight: 500;
  margin-left: auto;
`;

const Username = styled.p`
  font-weight: 600;
  color: #fd7e14;
`;