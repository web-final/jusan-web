import styled from "styled-components";
import {Header} from "./components/Header";
import React, {FC} from "react";
import {Footer} from "./components/Footer";

type Props = {
    children: React.ReactNode;
}
export const MainLayout: FC<Props> = ({children}) => {
    return (
        <Wrapper>
            <Header />
            <Main>
                {children}
            </Main>
            <Footer />
        </Wrapper>
    )
}

const Wrapper = styled('div')`
    min-height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const Main = styled('div')`
    flex: 1;
  padding: 40px 0;
`;