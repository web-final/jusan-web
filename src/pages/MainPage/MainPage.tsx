import styled from "styled-components";
import {MainLayout} from "../../layouts/MainLayout/MainLayout";
import {Banner} from "./components/Banner";
import {PopularNavigation} from "./components/PopularNavigation";
import {Products} from "./components/Products";
import { Shop } from "./components/Shop";
import {Services} from "./components/Services";

export const MainPage = () => {
    return (
        <MainLayout>
            <Banner />
            <PopularNavigation />
            <Products />
            <Shop />
            <Services />
        </MainLayout>
    )
}


export const Wrapper = styled('div')``;