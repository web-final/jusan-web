import styled from "styled-components";

export const Services = () => {
    return (
        <Wrapper className="container">
            <Title className="title">Сервисы</Title>
            <Container>
                <ItemOne>
                    <ItemTop>
                        <ItemTitle>Банк для малого и среднего бизнеса</ItemTitle>
                        <ItemSubtitle>У нас всё онлайн — на сайте и в приложении. Откройте счет не выходя из дома и ведите бизнес с комфортом</ItemSubtitle>
                    </ItemTop>
                    <ItemButton>Перейти</ItemButton>
                    <Img src="https://jusan.kz/file-server/filename?dir=services&filename=business.webp" />
                </ItemOne>
                <ItemTwo>
                    <ItemTop>
                        <ItemTitle>Инвестиции</ItemTitle>
                        <ItemSubtitle>Инвестируйте или доверьте экспертам управлять вашими инвестициями</ItemSubtitle>
                    </ItemTop>
                    <ItemButton>Перейти</ItemButton>
                    <Img src="https://jusan.kz/file-server/filename?dir=services&filename=invest-1.webp" />
                </ItemTwo>
                <ItemThree>
                    <ItemTop>
                        <ItemTitle>Страхование</ItemTitle>
                        <ItemSubtitle>Мы оберегаем все, что Вы цените</ItemSubtitle>
                    </ItemTop>
                    <ItemButton>Перейти</ItemButton>
                    <Img src="https://jusan.kz/file-server/filename?dir=services&filename=invest.webp" />
                </ItemThree>
            </Container>
        </Wrapper>
    )
}

const Wrapper = styled.div``;

const Title = styled.h2``;

const Container = styled.div`
    grid-gap: 30px;
    display: grid;
    grid-template-columns: repeat(2,1fr);
`;

const Item = styled.div`
    border-radius: 24px;
    color: #fff;
    height: 376px;
    justify-content: space-between;
    overflow: hidden;
    padding: 35px;
    position: relative;
    text-decoration: none;
    z-index: 1;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
`

const ItemOne = styled(Item)`
    grid-column: 1/3;
    background: radial-gradient(73.2% 135.36% at 74.15% 69.72%,#898e93 0,#4f4f4f 100%),#6f6d6d;
`

const ItemTwo = styled(Item)`
    background: radial-gradient(73.2% 135.36% at 74.15% 69.72%,#ff7b51 0,#ff4f4f 100%),#ffc63d;
    height: 300px;
`;

const ItemThree = styled(Item)`
    background: radial-gradient(73.2% 135.36% at 74.15% 69.72%,#ff9357 0,#ff5c2b 100%),#ffc63d;
  height: 300px;
`;

const ItemTop = styled.div`
    
`

const ItemTitle = styled.h3`
    font-size: 25px;
    font-weight: 700
`

const ItemSubtitle = styled.h4`
    max-width: 100%;
    width: 330px;
`

const ItemButton = styled.button`
    background: #fff;
    border-radius: 50px;
    color: #201f1f;
    padding: 18px 35px;
    transition: .3s ease;
    border: none;
`

const Img = styled.img`
    bottom: 0;
    height: 100%;
    object-fit: cover;
    position: absolute;
    right: 0;
    transition: .3s linear;
    width: 50%;
    z-index: -1;
  vertical-align: middle;
  
`