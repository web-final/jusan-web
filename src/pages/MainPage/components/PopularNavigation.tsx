import styled from "styled-components";

export const PopularNavigation = () => {
    return (
        <Wrapper className="container">
                <NavItem>
                    <NavIcon src="https://jusan.kz/file-server/filename?dir=popular-navigations/main-navigation&filename=nav-second-stock.webp" />
                    <NavText>Акции</NavText>
                </NavItem>
                <NavItem>
                    <NavIcon src="https://jusan.kz/file-server/filename?dir=popular-navigations/main-navigation&filename=nav-second-express-loan.webp" />
                    <NavText>Кредиты</NavText>
                </NavItem>
                <NavItem>
                    <NavIcon src="https://jusan.kz/file-server/filename?dir=icons&filename=pay-cart-icon.webp" />
                    <NavText>Дебетовые карты</NavText>
                </NavItem>
                <NavItem>
                    <NavIcon src="https://jusan.kz/file-server/filename?dir=icons&filename=auto-inshurance-icon.webp" />
                    <NavText>Автострахование</NavText>
                </NavItem>
                <NavItem>
                    <NavIcon src="https://jusan.kz/file-server/filename?dir=popular-navigations/main-navigation&filename=nav-second-jusan-deposit.webp" />
                    <NavText>Депозиты</NavText>
                </NavItem>
        </Wrapper>
    )
}


const Wrapper = styled('div')`
    background: #fff;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 12px 15px;
  margin: 32px auto;
  border-radius: 24px;
`;


const NavItem = styled('div')`
    display: flex;
  align-items: center;
  column-gap: 12px;
`;

const NavIcon = styled('img')`
    width: 40px;
    height: 40px;
`;

const NavText = styled('span')`
  color: #201f1f;
  transition: all .4s ease;
  white-space: nowrap;
`;