import { Swiper, SwiperSlide } from 'swiper/react';

import {Navigation, Autoplay} from "swiper";

export const Banner = () => {
    return (
        <Swiper
            navigation
            spaceBetween={50}
            slidesPerView={1}
            centeredSlides
            className="container"
            style={{background:'#f6f6f5'}}
            modules={[Navigation, Autoplay]}
            autoplay={{ delay: 1700 }}
            loop
        >
            <SwiperSlide style={{ borderRadius: '24px', width: '100%', margin:'0 auto', height: '100%'}}>
                <img src="https://jusan.kz/file-server/filename?dir=slider&filename=BARISTO-1440x374-ru-min.webp" alt=""/>
            </SwiperSlide>
            <SwiperSlide style={{ borderRadius: '24px', width: '100%', margin:'0 auto', height: '100%'}}>
                <img  src="https://jusan.kz/file-server/filename?dir=banner&filename=1440x374_РУС-min.webp" alt=""/>
            </SwiperSlide>
            <SwiperSlide style={{ borderRadius: '24px', width: '100%', margin:'0 auto', height: '100%'}}>
            <img  src="https://jusan.kz/file-server/filename?dir=slider&filename=business-car-desktop-ru.webp" alt=""/>
        </SwiperSlide>
        </Swiper>
    );
}