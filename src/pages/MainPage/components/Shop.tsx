import styled from "styled-components";
import {useNavigate} from "react-router";

export const Shop = () => {
    const navigate = useNavigate();

    const items = [
        {title: 'Спорт и туризм', img: 'https://jusan.kz/file-server/filename?dir=market&filename=sportandtourism.webp'},
        {title: 'Ноутбуки и Компьютеры', img: 'https://jusan.kz/file-server/filename?dir=market&filename=notebook.webp'},
        {title: 'Смартфоны и гаджеты', img: 'https://jusan.kz/file-server/filename?dir=market&filename=smartphone.webp'},
        {title: 'Мебель для дома', img: 'https://jusan.kz/file-server/filename?dir=market&filename=furniture.webp'},
        {title: 'Бытовая техника', img: 'https://jusan.kz/file-server/filename?dir=market&filename=appliances.webp'},
        {title: 'ТВ, аудио, видео и фото', img: 'https://jusan.kz/file-server/filename?dir=market&filename=educational.webp'},
    ]
    return (
        <Wrapper className="container">
            <Header>
                <Title className="title">Магазин</Title>
                <Button className="arrow-more" onClick={() => {navigate('/shop')}}>
                    <svg viewBox="0 0 18 18" fill="black" className="ic" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                              d="M11.2293 7.49979L8.18009 4.19646L10.3845 2.16162L16.6967 8.99979L10.3845 15.838L8.18009 13.8031L11.2293 10.4998H3.01379V7.49979H11.2293Z"></path>
                    </svg>
                </Button>
            </Header>
            <List>
                {items.map((item, idx) => (
                    <Item key={idx}>
                        <ItemTitle>{item.title}</ItemTitle>
                        <Img src={item.img} />
                    </Item>
                ))}
            </List>
        </Wrapper>
    )
}

const Wrapper = styled.div`
    margin-bottom: 36px;
`;
const Header = styled.div`
    display: flex;
  justify-content: space-between;
  align-items: center;
`;

const Button = styled.button``;


const Title = styled('h2')`
`

const List = styled.div`
  grid-gap: 30px;
  display: grid;
  grid-template-columns: repeat(3,1fr);
  margin-bottom: 30px;
`

const Item = styled.div`
    background-color: #fff;
    border-radius: 24px;
    color: #000;
    height: 220px;
    overflow: hidden;
    padding: 30px;
    position: relative;
    text-decoration: none;
    z-index: 1;
  display: flex;
    justify-content: flex-end!important;
  
`;

const ItemTitle = styled.h3`
    color: #201f1f;
    font-size: 21px;
    font-weight: 500;
    text-align: right;
    transition: .3s ease;
    width: 146px
`

const Img = styled.img`
  bottom: 0;
  height: 80%;
  left: 0;
  object-fit: cover;
  position: absolute;
  transition: .3s linear;
  width: 70%;
  z-index: -1;
`