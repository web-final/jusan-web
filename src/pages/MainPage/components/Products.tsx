import styled from "styled-components";

export const Products = () => {
    return (
            <Wrapper className="container">
                <Title className="title"></Title>
                <ItemList>
                    <ItemOne>
                        <ItemTop>
                            <ItemTitle>Экспресс-кредит</ItemTitle>
                            <ItemSubtitle>Онлайн без залога и на любые цели</ItemSubtitle>
                        </ItemTop>
                        <ImageBlock>
                            <img src="https://jusan.kz/lottie/credit/images/img_1.png" alt=""/>
                        </ImageBlock>
                    </ItemOne>
                    <ItemTwo>
                        <ItemTop>
                            <ItemTitle>Jusan депозит</ItemTitle>
                            <ItemSubtitle>Управляйте своими сбережениями онлайн</ItemSubtitle>
                        </ItemTop>
                        <ImageBlock>
                            <img src="https://jusan.kz/lottie/deposit/images/img_1.png" alt=""/>
                        </ImageBlock>
                    </ItemTwo>
                    <ItemThree>
                        <ItemTop>
                            <ItemTitle>Карта Jusan</ItemTitle>
                            <ItemSubtitle>Чем больше транзакций, тем больше бонусов</ItemSubtitle>
                        </ItemTop>
                        <ImageBlock>
                            <img src="https://jusan.kz/lottie/card/images/img_1.png" alt=""/>
                        </ImageBlock>
                    </ItemThree>
                    <ItemFour>
                        <ItemTop>
                            <ItemTitle>Рефинансирование займов</ItemTitle>
                            <ItemSubtitle>Выгодные условия для комфортного погашения</ItemSubtitle>
                        </ItemTop>
                        <ImageBlock>
                            <img src="https://jusan.kz/lottie/refinansing/images/img_1.png" alt=""/>
                        </ImageBlock>
                    </ItemFour>
                    <ItemFive>
                        <ItemTop>
                            <ItemTitle>Кредит до зарплаты*</ItemTitle>
                            <ItemSubtitle>Выдаем нужную сумму до зарплаты. Без залога, на любые цели.</ItemSubtitle>
                        </ItemTop>
                        <ImageBlock>
                            <img src="https://jusan.kz/lottie/credit-shop/images/img_1.png" alt=""/>
                        </ImageBlock>
                    </ItemFive>
                </ItemList>
            </Wrapper>
    )
}

const Wrapper = styled.div`
 margin-bottom: 36px;
`;

const ItemList = styled.div`
  grid-gap: 30px;
  display: grid;
  grid-template-columns: repeat(6,1fr);
`;

const Item = styled.div`
    border-radius: 24px;
    color: #201f1f;
    height: 376px;
    padding: 35px;
    position: relative;
    text-decoration: none;
    z-index: 1;
      margin: 10px;
      background: white;
`;



const ItemOne = styled(Item)`
  grid-column: 1/3;
`;

const ItemTwo = styled(Item)`
  grid-column: 3/5;
`;

const ItemThree = styled(Item)`
  grid-column: 5/7;
`
const ItemFour = styled(Item)`
  grid-column: 1/4;

`

const ItemFive = styled(Item)`
  grid-column: 4/7;
`
const ItemTop = styled.div`
    
`;

const ItemTitle = styled.h3`
    font-size: 21px;
  font-weight: 700;
  margin-bottom: 8px;
`

const ItemSubtitle = styled.h4`
    color: #898d94;
    font-size: 15px;
    max-width: 320px;
`

const ImageBlock = styled.div`
    height: 250px;
    object-fit: contain;
    position: absolute;
    right: 0;
    width: 250px;
    z-index: -1;
    bottom: 45px;
    & img {
      width: 100%;
    }
`;


const Title = styled.h2``;