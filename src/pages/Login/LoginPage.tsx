import React from 'react';
import './LoginPage.css';


function LoginPage() { 
  return (
    <div className="login-wrap">
      <div className="logo">
        <img src="./assets/logo-jusanbusiness.svg" alt="Jusan Business" />
      </div>
      <div className="login-form">
        <section className="login-steps">
          <article>
            <h3>Вход</h3>
            <p>в интернет-банкинг для бизнеса</p>
          </article>
          <form autoComplete="off">
            <div className="phone-input">
              <button type="button" className="flag-select-btn">
                <img className="flag-icon" src="./assets/kz-flag-icon.png" />
              </button>
              <input className="phone-number" autoComplete="off" placeholder="+7 (_ _ _) _ _ _  _ _  _ _"
                onKeyPress={(event) => {
                    if (!/[0-9]/.test(event.key)) {
                    event.preventDefault();
                    }
                }}
              />
            </div>
            <div className="password-input">
              <input type="password" className="pwd" placeholder="Пароль" autoComplete="new-password" />
            </div>
            <div className="restore-pwd">
              <a href="https://ib.jusan.kz/auth/login" target={"_blank"}>
                Забыли пароль?
              </a>
            </div> 
            <div className="auth-submit">
              <button type="submit" className="login-btn">
                Войти
              </button>
              <button className="register-btn">
                Зарегистрироваться
              </button>
            </div>
          </form>
        </section> 
        </div>
        <nav className="lang-list">
          <a href="#" className="ru-lang">
            Рус
          </a>
          <a href="#" className="lang">
            Қаз
          </a>
          <a href="#" className="lang">
            Eng
          </a>
        </nav>
        <p className="copyright">
          © 1992-2022 гг. <br></br>
          АО «First Heartland Jusan Bank»
        </p>
    </div>
  );
}

export default LoginPage;