import React from "react";
import "./MobileNetCat.css";

function MobileNetCat() {
  return (
    <div className="wrapper">
      <h1 className="article">Мобильная связь</h1>
        <div className="categories">
          <img src="./assets/jusan-mob-icon.jpg" className="payment-icon" />
          <h3 className="category-name">Jusan Mobile</h3>
        </div> <br></br>
        <div className="categories">
          <img src="./assets/activ-icon.jpg" className="payment-icon" />
          <h3 className="category-name">Activ</h3>
        </div> <br></br>
        <div className="categories">
          <img src="./assets/beeline-icon.jpg" className="payment-icon" />
          <h3 className="category-name">Beeline</h3>
        </div> 
    </div>
  );
}

export default MobileNetCat;