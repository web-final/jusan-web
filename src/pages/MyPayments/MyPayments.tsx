import styled from "styled-components";
import {MainLayout} from "../../layouts/MainLayout/MainLayout";

export const MyPayments = () => {
    return (
        <MainLayout>
            <Wrapper className="container">
                <Title>Мои платежи</Title>
            </Wrapper>
        </MainLayout>
    )
}

const Wrapper = styled.div`
`;

const Title = styled.h1`
    font-weight: 700;
    font-size: 48px;
`;