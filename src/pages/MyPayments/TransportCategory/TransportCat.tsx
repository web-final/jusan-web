import React from "react";
import "./TransportCat.css";

function TransportCat() {
  return (
    <div className="wrapper">
      <h1 className="article">Транспорт и Билеты</h1>
        <div className="categories">
          <img src="./assets/bus-icon.jpg" className="payment-icon" />
          <h3 className="category-name">Общественный транспорт</h3>
        </div> <br></br>
        <div className="categories">
          <img src="./assets/parking-icon.jpg" className="payment-icon" />
          <h3 className="category-name">Парковки</h3>
        </div> <br></br>
        <div className="categories">
          <img src="./assets/taxi-icon.jpg" className="payment-icon" />
          <h3 className="category-name">Такси</h3>
        </div> 
    </div>
  );
}

export default TransportCat;