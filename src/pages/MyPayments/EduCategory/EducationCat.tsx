import React from "react";
import "./EducationCat.css";

function EducationCat() {
  return (
    <div className="wrapper">
      <h1 className="article">Образование</h1>
        <div className="categories">
          <img src="./assets/kinder-icon.jpg" className="payment-icon" />
          <h3 className="category-name">Детские Сады</h3>
        </div> <br></br>
        <div className="categories">
          <img src="./assets/schools-icon.jpg" className="payment-icon" />
          <h3 className="category-name">Школы</h3>
        </div> <br></br>
        <div className="categories">
          <img src="./assets/university-icon.jpg" className="payment-icon" />
          <h3 className="category-name">ВУЗы и Колледжи</h3>
        </div> 
    </div>
  );
}

export default EducationCat;