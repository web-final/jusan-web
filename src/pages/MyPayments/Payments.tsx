import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faAngleDown, faClockRotateLeft, faSearch} from "@fortawesome/free-solid-svg-icons";
import "./Payments.css";
import { NavLink } from "react-router-dom";

function Payments() {
  return (
    <div className="wrapper">
      <header>
        <div className="top-section">
          <h1 className="article">Платежи</h1>
          <input type="text" className="search-bar" placeholder="Что хотите оплатить?" />
          <FontAwesomeIcon icon={faSearch} className="search-btn" />
          <img src="./assets/notification-icon.jpg" className="not-btn" />
          <img src="./assets/profile-icon.jpg" className="prof-btn"/>
        </div>
        <div className="bottom-section">
          <button type="button" className="payment-history-btn">
            <FontAwesomeIcon icon={faClockRotateLeft} className="history-btn" /> 
            История платежей
          </button>
          <button type="button" className="choose-city-btn">
            Алматы
            <FontAwesomeIcon icon={faAngleDown} className="dropdown-btn" />
          </button>
        </div>     
      </header>
      <main>
        <div className="main-content">
          <NavLink to="/mobile-network">
          <div className="categories">
              <img src="./assets/mobile-network-icon.jpg" className="payment-icon" />
              <h3 className="category-name">Мобильная связь</h3>
            </div> <br></br>
          </NavLink>
          <div className="categories">
            <img src="./assets/ut-services-icon.jpg" className="payment-icon" />
            <h3 className="category-name">Коммунальные службы</h3>
          </div> <br></br>
          <div className="categories">
            <img src="./assets/fines-icon.jpg" className="payment-icon" />
            <h3 className="category-name">Штрафы и Налоги</h3>
          </div> <br></br>
          <NavLink to="/education">
          <div className="categories">
            <img src="./assets/edu-icon.jpg" className="payment-icon" />
            <h3 className="category-name">Образование</h3>
          </div> <br></br>
          </NavLink>
          <NavLink to="/transport">
          <div className="categories">
            <img src="./assets/transport-icon.jpg" className="payment-icon" />
            <h3 className="category-name">Транспорт и Билеты</h3>
          </div> <br></br>
          </NavLink>
          <div className="categories">
            <img src="./assets/finance-icon.jpg" className="payment-icon" />
            <h3 className="category-name">Финансовые услуги</h3>
          </div> <br></br>
          <div className="categories">
            <img src="./assets/health-icon.jpg" className="payment-icon" />
            <h3 className="category-name">Красота и Здоровье</h3>
          </div> <br></br>
          <div className="categories">
            <img src="./assets/relax-icon.jpg" className="payment-icon" />
            <h3 className="category-name">Развлечения и Отдых</h3>
          </div> <br></br>
          <div className="categories">
            <img src="./assets/betting-icon.jpg" className="payment-icon" />
            <h3 className="category-name">Букмекерские компании</h3>
          </div> <br></br>
          <div className="categories">
            <img src="./assets/charity-icon.jpg" className="payment-icon" />
            <h3 className="category-name">Благотворительность</h3>
          </div> <br></br>
          <div className="categories">
            <img src="./assets/others-icon.jpg" className="payment-icon" />
            <h3 className="category-name">Прочие услуги</h3>
          </div> <br></br>
        </div>
      </main>
    </div>
  );
}

export default Payments;