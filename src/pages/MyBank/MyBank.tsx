import {MainLayout} from "../../layouts/MainLayout/MainLayout";
import styled from "styled-components";

export const MyBank = () => {
    return (
        <MainLayout>
            <Wrapper className="container">
                <Title>Мой банк</Title>
                <div style={{
                    display: "flex",
                    gap: "10px",
                    marginTop: "30px"
                }}>
                <div style={{
                    borderRadius: "30px",
                    width: "350px",
                    height: "200px",
                    backgroundColor: "red",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center"
                }}>
                    <p style={{
                        color: "#fff",
                        backgroundColor: "transparent",
                        fontWeight: "bold"
                       
                    }}>Ваш баланс равен 250000 тг</p> 

                </div>
                <div style={{
                    borderRadius: "30px",
                    width: "350px",
                    height: "200px",
                    backgroundColor: "grey",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center"
                }}>
                    <p style={{
                        color: "#fff",
                        backgroundColor: "transparent",
                        fontWeight: "bold"
                       
                    }}>Счет ABC1235879degh</p> 

                </div>
                </div>
            </Wrapper>
        </MainLayout>
    )
}

const Wrapper = styled.div`
`;

const Title = styled.h1`
    font-weight: 700;
    font-size: 48px;
`;