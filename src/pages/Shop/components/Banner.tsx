import { Swiper, SwiperSlide } from 'swiper/react';
import {Navigation, Autoplay} from "swiper";

export const Banner = () => {
    const imgs = [
        'https://jmart.kz/images/detailed/4530/image_new_admin_6396b69e8763f0.81043655.png',
        'https://jmart.kz/images/detailed/4530/image_new_admin_63931fb571d694.31253675.png',
        'https://jmart.kz/images/detailed/4530/image_new_admin_63931fb2136142.33550950.png',
        'https://jmart.kz/images/detailed/4566/image_new_admin_63a97686699ea7.42611508.png',
        'https://jmart.kz/images/detailed/4530/image_new_admin_63931fb4ddeff7.42979004.png',
    ]
    return (
        <Swiper
            navigation
            spaceBetween={50}
            slidesPerView={1}
            centeredSlides
            className="container"
            style={{background:'#f6f6f5'}}
            modules={[Navigation, Autoplay]}
            autoplay={{ delay: 1700 }}
            loop
        >
            <SwiperSlide style={{ borderRadius: '24px', width: '100%', margin:'0 auto', height: '100%'}}>
                <img src="https://jmart.kz/images/detailed/4530/image_new_admin_6396b69e8763f0.81043655.png" alt=""/>
            </SwiperSlide>
            <SwiperSlide style={{ borderRadius: '24px', width: '100%', margin:'0 auto', height: '100%'}}>
                <img  src="https://jmart.kz/images/detailed/4530/image_new_admin_63931fb571d694.31253675.png" alt=""/>
            </SwiperSlide>
            <SwiperSlide style={{ borderRadius: '24px', width: '100%', margin:'0 auto', height: '100%'}}>
                <img  src="https://jmart.kz/images/detailed/4530/image_new_admin_63931fb2136142.33550950.png" alt=""/>
            </SwiperSlide>
        </Swiper>
    );
}