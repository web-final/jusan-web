import styled from "styled-components";
import {MainLayout} from "../../layouts/MainLayout/MainLayout";
import {Banner} from "./components/Banner";

export const Shop = () => {
    return (
        <MainLayout>
            <Wrapper>
               <Banner></Banner>
            </Wrapper>
        </MainLayout>
    )
}

const Wrapper = styled.div`
`;

const Title = styled.h1`
    font-weight: 700;
    font-size: 48px;
`;