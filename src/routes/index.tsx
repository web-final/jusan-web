import {createBrowserRouter} from "react-router-dom";
import React from "react";
import {MainPage} from "../pages/MainPage/MainPage";
import {MyBank} from "../pages/MyBank/MyBank";
import { Shop } from "../pages/Shop/Shop";
import LoginPage from "../pages/Login/LoginPage";
import Payments from "../pages/MyPayments/Payments";
import MobileNetCat from "../pages/MyPayments/MobNetCategory/MobileNetCat";
import EducationCat from "../pages/MyPayments/EduCategory/EducationCat";
import TransportCat from "../pages/MyPayments/TransportCategory/TransportCat";

export const router = createBrowserRouter([
    {
        path: "/",
        element: <MainPage />,
    },
    {
        path: "/my-payments",
        element: <Payments />,
    },
    {
        path: "/mobile-network",
        element: <MobileNetCat />,
    },
    {
        path: "/education",
        element: <EducationCat />,
    },
    {
        path: "/transport",
        element: <TransportCat />,
    },
    {
        path: "/my-bank",
        element: <MyBank />,
    },
    {
        path: '/shop',
        element: <Shop />
    },
    {
        path: '/login',
        element: <LoginPage />
    },
]);