export type LoginDto = {
    username: string;
    password: string;
}

export enum ProductCategory {
    PHONE = 'PHONE',
    JEWELERY = 'JEWELERY',
    HOME = 'HOME'
}