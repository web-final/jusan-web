import React from 'react';
import './App.css';
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import {router} from "./routes";
import 'swiper/swiper.min.css'
import './swiper-styles/pagination.scss'; // Pagination module
import  './swiper-styles/navigation.scss';

function App() {
  return (
       <RouterProvider router={router} />
  );
}

export default App;
